package com.czechowski.zadani1;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


/**
 * @author <a href="mailto:k.czechowski83@gmail.com">Krzysztof Czechowski</a>
 */
public class NaszIntegerUtils {

    public List<Integer> getAllIntegerBetween_0_To_10_FromList(List<Integer> integers) {

        if (integers == null) {
            return new ArrayList<>();
        }
        return integers.stream()
                .filter(i -> (i != null && i <= 10 && i > 0))
                .collect(Collectors.toList());

    }
}
