package com.czechowski.zadani1;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author <a href="mailto:k.czechowski83@gmail.com">Krzysztof Czechowski</a>
 */
class NaszIntegerUtilsTest {

    private NaszIntegerUtils naszIntegerUtils = new NaszIntegerUtils();


    @Test
    void shouldReturnAllGivenIntegersForListRange_1_to_4() {


        List<Integer> integers = List.of(1, 2, 3, 4);
        List<Integer> list = naszIntegerUtils.getAllIntegerBetween_0_To_10_FromList(integers);


        assertEquals(list.size(), 4);
        assertArrayEquals(list.toArray(), integers.toArray());
    }

    @Test
    void shouldNotReturnNull() {

        List<Integer> integers = Arrays.asList(1, 2, 3, 4, null);
        List<Integer> expected = Arrays.asList(1, 2, 3, 4);
        List<Integer> list = naszIntegerUtils.getAllIntegerBetween_0_To_10_FromList(integers);

        assertEquals(list.size(), 4);
        assertArrayEquals(list.toArray(), expected.toArray());
    }

    @Test
    void shouldNotReturnIntegerLessThan_0() {

        List<Integer> integers = Arrays.asList(1, 2, 3, 4, -2);
        List<Integer> expected = Arrays.asList(1, 2, 3, 4);
        List<Integer> list = naszIntegerUtils.getAllIntegerBetween_0_To_10_FromList(integers);

        assertEquals(list.size(), 4);
        assertArrayEquals(list.toArray(), expected.toArray());
    }


    @Test
    void shouldNotReturnIntegerBiggerThan_10() {

        List<Integer> integers = Arrays.asList(1, 2, 3, 4, 123);
        List<Integer> expected = Arrays.asList(1, 2, 3, 4);
        List<Integer> list = naszIntegerUtils.getAllIntegerBetween_0_To_10_FromList(integers);

        assertEquals(list.size(), 4);
        assertArrayEquals(list.toArray(), expected.toArray());
    }

    @Test
    void shouldReturnEmptyListWhenGivenEmptyList() {

        List<Integer> integers = new ArrayList<>();
        List<Integer> list = naszIntegerUtils.getAllIntegerBetween_0_To_10_FromList(integers);

        assertNotNull(list);
        assertEquals(list.size(), 0);
    }

    @Test
    void shouldReturnEmptyListWhenGivenNull() {

        List<Integer> integers = null;
        List<Integer> list = naszIntegerUtils.getAllIntegerBetween_0_To_10_FromList(integers);

        assertNotNull(list);
        assertEquals(list.size(), 0);
    }

    @Test
    void shouldReturnEmptyListWhenGivenListWithNullAndIntegerBiggerThan_10_And_Integer() {

        List<Integer> integers = Arrays.asList(null, 123, 3456);
        List<Integer> list = naszIntegerUtils.getAllIntegerBetween_0_To_10_FromList(integers);

        assertNotNull(list);
        assertEquals(list.size(), 0);
    }
}
